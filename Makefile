.DEFAULT_GOAL := help

help: ## show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)


start: ## start project
	docker-compose up -d --remove-orphans

stop: ## stop project
	docker-compose down

restart: stop start ## restart project